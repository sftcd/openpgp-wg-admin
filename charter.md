OpenPGP is an Internet standard that covers object encryption, object signing, and identity certification.

The working group is chartered to work on improvements and additions to the OpenPGP format and ecosystem to address certain issues that have been identified by the community, as set out in the list of "in scope" topics below. Due to the WG having been dormant for a number of years, there is somewhat of a "backlog" of topics, and as addressing all of these topics at once seems difficult, the WG will follow the process defined below to prioritize current lists of milestones, selected from this long list of "in-scope" topics.

# In-scope Topics

These improvements may include:

- Security improvements:

  - The addition and facilitation of post-quantum algorithms for encryption and signing; e.g. by adopting [draft-wussler-openpgp-pqc](https://datatracker.ietf.org/doc/draft-wussler-openpgp-pqc/).

  - Forward secrecy.


- Facilitating new functionality:

  - Automatic forwarding, using proxy re-encryption; e.g. by adopting [draft-wussler-openpgp-forwarding](https://datatracker.ietf.org/doc/draft-wussler-openpgp-forwarding/).

  - Persistent symmetric keys, for long-term storage of symmetric key material, symmetrically encrypted messages, and symmetric attestations; e.g. by adopting [draft-huigens-openpgp-persistent-symmetric-keys](https://datatracker.ietf.org/doc/draft-huigens-openpgp-persistent-symmetric-keys/).

  - Designated Revoker, to replace the deprecated Revocation Key mechanism.

  - Attestation Signatures, to facilitate first-party-attested third-party certifications (1PA3PC).

  - Superseded keys, to facilitate transition to new keys.

  - Stateless OpenPGP Interface (SOP); e.g. by adopting [draft-dkg-openpgp-stateless-cli](https://datatracker.ietf.org/doc/draft-dkg-openpgp-stateless-cli/).

  - Extensions to [RFC 3156](https://datatracker.ietf.org/doc/rfc3156/) (PGP/MIME).

- Specifications of and improvements to network-based key discovery mechanisms:

  - HTTP Keyserver Protocol (HKP); e.g. by adopting and extending [draft-shaw-openpgp-hkp](https://datatracker.ietf.org/doc/draft-shaw-openpgp-hkp/).

  - Web Key Directory (WKD); e.g. by adopting and extending [draft-koch-openpgp-webkey-service](https://datatracker.ietf.org/doc/draft-koch-openpgp-webkey-service/).

- Key verification mechanisms:

  - Key Transparency, in collaboration with the [Key Transparency Working Group](https://datatracker.ietf.org/wg/keytrans/about/), e.g. integrating its outputs.

  - Improved manual key verification, for example using a QR code.

- Miscellaneous cleanup work:

  - Properly document User ID conventions.

  - [Simplify](https://mailarchive.ietf.org/arch/msg/openpgp/uepOF6XpSegMO4c59tt9e5H1i4g/) the OpenPGP Message Syntax

The working group will produce a number of specifications that are adjacent to the OpenPGP specification and provide guidance to OpenPGP libraries and/or applications to achieve the aforementioned improvements.

# Working Group Process

The working group will endeavor to complete most of its work online on the working group's mailing list.
The group may hold face-to-face sessions at IETF meetings, or interim calls, whenever necessary or useful.

For all work items, any content requires both rough consensus on the mailing list and the demonstration of interoperable support by at least two independent implementations, before being submitted to the IESG.

The WG chairs will periodically poll the WG for which of the topics above to add as milestones, as participant interest, document editor, review and implementer resources permit. The WG chairs will ensure that the list of active topics is credible at all times, likely resulting in only 3-4 topics being "active" at any given time. Such polls will typically result in the addition of a call-for-adoption for one or two new topics from the list above, depending on resources, and as currently active work is completed. "Completion" will typically mean that a draft has passed working group last call or IETF last call, but, if needed, the chairs will make use of other datatracker [IETF document states](https://datatracker.ietf.org/doc/help/state/draft-stream-ietf/), e.g. "parked WG document", if some document is stalled for technical or personnel reasons, leaving space for tackling anther topic.

